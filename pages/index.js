import Head from 'next/head'
import { useEffect, useState } from 'react'
import SCPTile from '../components/SCPTile/SCPTile';
import styles from '../styles/Home.module.scss'

export default function Home() {

  const [data, setData] = useState();

  useEffect(() => {
    fetch("/api/get/scp")
      .then(x => x.json())
      .then(x => {
        setData(x);
      })
  },[]);

  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div>
        {data && data.map((x, k) => <SCPTile key={k} data={x} />)}
      </div>
    </div>
  )
}
