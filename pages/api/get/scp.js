import listScps from "../../../utils/data/getScps";

export default async (req, res) => {
  const data = await listScps(req.query);
  res.json(data);
};
