import TurndownService from "turndown";

const scrape = async ({id}) => {
	const converter = new TurndownService(),
		rawData = await fetch(`http://www.scpwiki.com/scp-${id}`),
		data = await rawData.text(),
		SCP = {};

	let content = data.match(/<div id="page-content">[\n\s\S]*<div class="footer-wikiwalk-nav">/gm)[0]; // get the content in the container
		content = converter.turndown(content); // convert to markdown

	// extract variables
	// image and caption
	const img = content.match(/!\[[^\]]*\]\(http[^\)]*\)[\n]*[^\n]*/gm);
	if (img) {
		SCP.img = img.map(x => {
			return {
				url: x.replace(/!\[[^\]]*\]\(/g, "").replace(/\)[\d\D]*/g, ""),
				caption: x.replace(/[^\)]*\)[\n]*/gm, "")
			}
		});
		// content = content.replace(/!\[[^\]]*\]\(http[^\)]*\)[\n]*[^\n]*/gm, ""); // remove from content
	}

	// SCP number
	SCP.number = content.match(/\*\*Item #:\*\* SCP-[0-9]*/g);
	SCP.number = SCP.number[0];
	SCP.number = SCP.number.replace(/\*\*Item #:\*\* SCP-/g, "");
	// content = content.replace(/\*\*Item #:\*\* SCP-[0-9]*/g, ""); // remove from content

	// Class
	SCP.class = content.match(/\*\*Object Class:\*\* [a-zA-Z]*/g);
	SCP.class = SCP.class[0];
	SCP.class = SCP.class.replace(/\*\*Object Class:\*\* /g, "");
	// content = content.replace(/\*\*Object Class:\*\* [a-zA-Z]*/g, ""); // remove from content

	// Containment procedures
	SCP.containmentProcedures = content.match(/\*\*Special Containment Procedures:\*\* [\d\D]*\*\*Description:/g);
	SCP.containmentProcedures = SCP.containmentProcedures[0];
	SCP.containmentProcedures = SCP.containmentProcedures.replace(/\*\*Special Containment Procedures:\*\* /g, "").replace(/\*\*Description:/g, "");
	// content = content.replace(/\*\*Object Class:\*\* [a-zA-Z]*/g, ""); // remove the content

	// Description
	SCP.description = content.match(/\*\*Description:\*\* [\d\D]*/g);
	SCP.description = SCP.description[0];
	SCP.description = SCP.description.replace(/\*\*Description:\*\* /g, "");

	// Sites
	SCP.sites = content.match(/(Bio[\s-]|)Site[\s-][\d]*/gm);

	// Sector
	SCP.sectors = content.match(/(Research[\s-]|)Sector[\s-][\d]*/gm);

	// Tags
	let tags = data.match(/page-tags">[\n\s]*<span>[\n\s]*(?:(?!\/span).)*/gm);
	if (tags) {
		tags = tags[0];
		tags = tags.match(/<a[^\<]*>[^\_][^\<]*/g);
		SCP.tags = tags.map(x => x.replace(/<a[^\<]*>/g,""))
	}

	// Updated
	SCP.updated = data.match(/odate[^\>]*>[^\(\<]*/gm);
	SCP.updated = SCP.updated[0];
	SCP.updated = new Date(SCP.updated.replace(/odate[^\>]*>/g,""));
	
	return SCP;
};

export default scrape;
