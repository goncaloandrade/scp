import connectToDatabase from "../db";

const listScps = async ({q, limit = 20, col = "scp"}) => {
	if (limit && typeof limit != "number") limit = parseInt(limit);
  const { db } = await connectToDatabase(),
  	$match = {};

	// Are we searching?
	if (q && q.length) {
		$match.name = new RegExp(q, 'g');
	}


  let data = await db
	.collection(col)
	.find($match)
	.limit(limit)
	.toArray();

  return data;
};

export default listScps;
